 /////////////////////////////////////////////////////////////////////////////////////////
 //
 // COMS20600 - WEEKS 3 and 4
 // ASSIGNMENT 1
 // CODE SKELETON
 // TITLE: "LED Ant Defender Game"
 //
 // - this is the first assessed piece of coursework in the unit
 // - this assignment is to be completed in pairs during week 3 and 4
 // - it is worth 10% of the unit (i.e. 20% of the course work component)
 //
 // OBJECTIVE: given a code skeleton with threads and channels setup for you,
 // implement a basic concurrent system on the XC-1A board
 //
 // NARRATIVE: You are given an XC code skeleton that provides you with
 // the structure and helper routines to implement a basic game on the
 // XC-1A board. Your task is to extend the given skeleton code to implement
 // the following game:
 //
 // An �LED Ant� is represented by a position on the clock wheel of the
 // XC-1A board. Each �LED Ant� is visualised by one active red LED on
 // the 12-position LED clock marked with LED labels I, II, II,�, XII.
 // No two LED Ants can have the same position on the clock. During the
 // game, the user has to defend LED positions I, XII and XI from an
 // LED Attacker Ant by controlling one LED Defender Ant and blocking the
 // attacker's path.
 //
 // Defender Ant
 // The user controls one �LED Ant� by pressing either button A (moving
 // 1 position clockwise) or button D (moving 1 position anti-clockwise).
 // The defender ant can only move to a position that is not already occupied
 // by the attacker ant. The defender�s starting position is LED XII. A sound
 // is played when the user presses a button.
 //
 // Attacker Ant
 // A second �LED Ant� is controlled by the system and starts at LED position VI.
 // It then attempts moving in one direction (either clockwise or anti-clockwise).
 // This attempt is denied if the defender ant is already located there, in this
 // case the attacker ant changes direction. To make the game more interesting:
 // before attempting the nth move, the attacker ant will change direction if n is
 // divisible by 23, 37 or 41. The game ends when the attacker has reached any one
 // of the LED positions I, XII or XI.
 //
 /////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <platform.h>
#include <time.h>
#include <stdlib.h>

out port cled0 = PORT_CLOCKLED_0;
out port cled1 = PORT_CLOCKLED_1;
out port cled2 = PORT_CLOCKLED_2;
out port cled3 = PORT_CLOCKLED_3;
out port cledG = PORT_CLOCKLED_SELG;
out port cledR = PORT_CLOCKLED_SELR;
in port buttons = PORT_BUTTON;
out port speaker = PORT_SPEAKER;

/////////////////////////////////////////////////////////////////////////////////////////
//
// Helper Functions provided for you
//
/////////////////////////////////////////////////////////////////////////////////////////


//WAIT function
void waitMoment(int n) {
    timer tmr;
    unsigned int waitTime;
    tmr :> waitTime;
    waitTime += n;
    tmr when timerafter(waitTime) :> void;
}


//This function generates the square wave that makes the piezo speaker sound at a determinated frequency.
void playOneSound(unsigned int note, unsigned int duration, out port speaker)
{
    int i,isOn = 1;
    long delay = (long)(10000/note);  //This is the semiperiod of each note.
    long t = (long)((duration*100)/(delay*2));  //This is how much time we need to spend on the note.
    for (i=0;i<t;i++)
    {
        isOn = !isOn;
        waitMoment(delay*1000);   //...for a semiperiod...
        speaker <: isOn;

        isOn = !isOn;
        waitMoment(delay*1000);   //...for the other semiperiod.
        speaker <: isOn;
    }
    waitMoment(5000000); //Add a little delay to separate the single notes
}


//DISPLAYS an LED pattern in one quadrant of the clock LEDs
int showLED(out port p, chanend fromVisualiser) {
     unsigned int lightUpPattern;
     while (1) {
     fromVisualiser :> lightUpPattern; //read LED pattern from visualiser process
     p <: lightUpPattern; //send pattern to LEDs
     }
     return 0;
}

void changeLed() {
    int t = 1;
    while(1) {
        cledR <: t;
        cledG <: !t;
        t = !t;
        waitMoment(50000);
    }
}

void playVisualiser (chanend toQuadrant0, chanend toQuadrant1, chanend toQuadrant2, chanend toQuadrant3) {
    int r = rand() % 6;
    int k;
    int a1[9] = {55068154, 55068163, 55068162, 40046349, 20001255, 55068163, 40046349, 20001255, 70097397};
    int a2[9] = {55115038, 55115046, 55115046, 40101350, 20004014, 55052421, 40050725, 20004014, 70107737};
    int a3[9] = {55168122, 35019259, 19987638, 45143356, 25059571, 25059571, 17518978, 17524224, 30063939};
    int a4[7] = {30038285, 45101348, 25039726, 25025053, 17490666, 17498155, 30019315};
    int a5[8] = {17506155, 55052421, 42518289, 17519104, 55076615, 42557112, 17490668, 70155546};

    int leds[7] = {16,32,64,48,80,96};
    int t = 0;
    for(int i = 0; i<9; i++) {
         cledG <: !t;
         cledR <: t;
         t = !t;
         k = a1[i] / 2;
         r = rand() % 6;
         toQuadrant0 <: leds[r];
         r = rand() % 6;
         toQuadrant1 <: leds[r];
         r = rand() % 6;
         toQuadrant2 <: leds[r];
         r = rand() % 6;
         toQuadrant3 <: leds[r];
         waitMoment(k);
         toQuadrant0 <: 0;
         toQuadrant1 <: 0;
         toQuadrant2 <: 0;
         toQuadrant3 <: 0;
         waitMoment(k);
    }
    waitMoment(5000000);
    for(int i = 0; i<9; i++) {
         cledG <: !t;
         cledR <: t;
         t = !t;
         k = a2[i] / 2;
         r = rand() % 6;
         toQuadrant0 <: leds[r];
         r = rand() % 6;
         toQuadrant1 <: leds[r];
         r = rand() % 6;
         toQuadrant2 <: leds[r];
         r = rand() % 6;
         toQuadrant3 <: leds[r];
         waitMoment(k);
         toQuadrant0 <: 0;
         toQuadrant1 <: 0;
         toQuadrant2 <: 0;
         toQuadrant3 <: 0;
         waitMoment(k);
    }
    waitMoment(5000000);
    for(int i = 0; i<9; i++) {
         cledG <: !t;
         cledR <: t;
         t = !t;
         k = a3[i] / 2;
         r = rand() % 6;
         toQuadrant0 <: leds[r];
         r = rand() % 6;
         toQuadrant1 <: leds[r];
         r = rand() % 6;
         toQuadrant2 <: leds[r];
         r = rand() % 6;
         toQuadrant3 <: leds[r];
         waitMoment(k);
         toQuadrant0 <: 0;
         toQuadrant1 <: 0;
         toQuadrant2 <: 0;
         toQuadrant3 <: 0;
         waitMoment(k);
    }
    waitMoment(5000000);
    for(int i = 0; i<7; i++) {
         cledG <: !t;
         cledR <: t;
         t = !t;
         k = a4[i] / 2;
         r = rand() % 6;
         toQuadrant0 <: leds[r];
         r = rand() % 6;
         toQuadrant1 <: leds[r];
         r = rand() % 6;
         toQuadrant2 <: leds[r];
         r = rand() % 6;
         toQuadrant3 <: leds[r];
         waitMoment(k);
         toQuadrant0 <: 0;
         toQuadrant1 <: 0;
         toQuadrant2 <: 0;
         toQuadrant3 <: 0;
         waitMoment(k);
    }
    waitMoment(5000000);
    for(int i = 0; i<8; i++) {
         cledG <: !t;
         cledR <: t;
         t = !t;
         k = a5[i] / 2;
         r = rand() % 6;
         toQuadrant0 <: leds[r];
         r = rand() % 6;
         toQuadrant1 <: leds[r];
         r = rand() % 6;
         toQuadrant2 <: leds[r];
         r = rand() % 6;
         toQuadrant3 <: leds[r];
         waitMoment(k);
         toQuadrant0 <: 0;
         toQuadrant1 <: 0;
         toQuadrant2 <: 0;
         toQuadrant3 <: 0;
         waitMoment(k);
    }
}

 //PROCESS TO COORDINATE DISPLAY of LED Ants
void visualiser(chanend fromUserAnt, chanend fromAttackerAnt, chanend toQuadrant0, chanend toQuadrant1, chanend toQuadrant2, chanend toQuadrant3, int colour) {
     unsigned int userAntToDisplay = 11;
     unsigned int attackerAntToDisplay = 5;
     int i, j, k = 1, t=0;
     int r = rand() % 6;
     int a1[9] = {55068154,55068163,55068162,40046349, 20001255, 55068163, 40046349, 20001255, 70097397};
     int leds[7] = {16,32,64,48,80,96};

     if (colour == 0) //Controlling the colour of the LED using a parameter
          cledG <: 1;
          else cledR <: 1;

    //playVisualiser(toQuadrant0, toQuadrant1, toQuadrant2, toQuadrant3); // Lights up the leds to make a visualiser for the Imperial March

    while (k == 1) {
        select {
            case fromUserAnt :> userAntToDisplay:{
                break;
            }
			case fromAttackerAnt :> attackerAntToDisplay:{
                break;
            }
        }
        if (attackerAntToDisplay == 11 || attackerAntToDisplay == 10 || attackerAntToDisplay == 0 || userAntToDisplay == 15) { // If the attacker enters a winning position
        	 j = 16 << (userAntToDisplay%3);
        	 i = 16 << (attackerAntToDisplay%3);
			 
            for(int i=0; i<10; ++i) {
                cledR <: 1;
                cledG <: 0;
                toQuadrant0 <: (j*(userAntToDisplay/3==0));
    		    toQuadrant1 <: (j*(userAntToDisplay/3==1));
    			toQuadrant2 <: (j*(userAntToDisplay/3==2));
    			toQuadrant3 <: (j*(userAntToDisplay/3==3));
    			waitMoment (20000000);
                toQuadrant0 <: 0;
                toQuadrant1 <: 0;
                toQuadrant2 <: 0;
                toQuadrant3 <: 0;
                waitMoment (20000000);
                cledR <: 0;
                cledG <: 1;
                if(attackerAntToDisplay == 0) {
                       toQuadrant0 <: 16;
                    }
                    else {
                        if (attackerAntToDisplay == 10) {
                            toQuadrant3 <: 32;
                        }
                    }
                waitMoment (20000000);
                toQuadrant0 <: 0;
                toQuadrant1 <: 0;
                toQuadrant2 <: 0;
                toQuadrant3 <: 0;
                waitMoment (20000000);
            }
            k = 0;
            break;
        }
        else { // if there is no winner, the game is played normally
			j = 16<<(userAntToDisplay%3);
			i = 16<<(attackerAntToDisplay%3);
			toQuadrant0 <: (j*(userAntToDisplay/3==0)) + (i*(attackerAntToDisplay/3==0)) ;
			toQuadrant1 <: (j*(userAntToDisplay/3==1)) + (i*(attackerAntToDisplay/3==1)) ;
			toQuadrant2 <: (j*(userAntToDisplay/3==2)) + (i*(attackerAntToDisplay/3==2)) ;
			toQuadrant3 <: (j*(userAntToDisplay/3==3)) + (i*(attackerAntToDisplay/3==3)) ;
        }
     }
     printf("Visualiser ends\n");
}

void play(out port spkr) {
    int a1[9], a2[9], a3[9], a4[7], a5[8];
    timer t;
    int t1,t2;
    int c=261;
    int d=294;
    int e=329;
    int f=349;
    int g =391;
    int gS =415;
    int a =440;
    int aS =455;
    int b =466;
    int cH= 523;
    int cSH= 554;
    int dH= 587;
    int dSH= 622;
    int eH= 659;
    int fH =698;
    int fSH= 740;
    int gH= 784;
    int gSH= 830;
    int aH =880;
    
    playOneSound(a, 500, spkr);
    playOneSound(a, 500, spkr);
    playOneSound(a, 500, spkr);
    playOneSound(f, 350, spkr);
    playOneSound(cH, 150, spkr);
    playOneSound(a, 500, spkr);
    playOneSound(f, 350, spkr);
    playOneSound(cH, 150, spkr);
    playOneSound(a, 650, spkr); // 9 to here
    
    waitMoment(5000000); //end of first bit

    playOneSound(eH, 500, spkr);
    playOneSound(eH, 500, spkr);
    playOneSound(eH, 500, spkr);
    playOneSound(fH, 350, spkr);
    playOneSound(cH, 150, spkr);
    playOneSound(gS, 500, spkr);
    playOneSound(f, 350, spkr);
    playOneSound(cH, 150, spkr);
    playOneSound(a, 650, spkr); // 9 here

    waitMoment(5000000); //end of second bit...

    playOneSound(aH, 500, spkr);
    playOneSound(a, 300, spkr);
    playOneSound(a, 150, spkr);
    playOneSound(aH, 400, spkr);
    playOneSound(gSH, 200, spkr);
    playOneSound(gH, 200, spkr);
    playOneSound(fSH, 125, spkr);
    playOneSound(fH, 125, spkr);
    playOneSound(fSH, 250, spkr); // 9 here

    waitMoment(5000000); // end of the third bit

    playOneSound(aS, 250, spkr);
    playOneSound(dSH, 400, spkr);
    playOneSound(dH, 200, spkr);
    playOneSound(cSH, 200, spkr);
    playOneSound(cH, 125, spkr);
    playOneSound(b, 125, spkr);
    playOneSound(cH, 250, spkr); // 7 here

    waitMoment(5000000); // end of the fourth bit

    playOneSound(f, 125, spkr);
    playOneSound(gS, 500, spkr);
    playOneSound(f, 375, spkr);
    playOneSound(a, 125, spkr);
    playOneSound(cH, 500, spkr);
    playOneSound(a, 375, spkr);
    playOneSound(cH, 125, spkr);
    playOneSound(eH, 650, spkr); // 8 here
 }

//PLAYS a short sound (pls use with caution and consideration to other students in the labs!)
void playSound(unsigned int wavelength, out port speaker) {
    timer tmr;
    int t, isOn = 1;
     tmr :> t;
     for (int i=0; i<2; i++) {
         isOn = !isOn;
         t += wavelength;
         tmr when timerafter(t) :> void;
         speaker <: isOn;
     }
}

//READ BUTTONS and send to userAnt
void buttonListener(in port b, out port spkr, chanend toUserAnt) {
     int r, k = 1;
     //play(spkr); //Playing the Imperial March on the Speaker (spkr)
     while (k == 1) {
     b when pinsneq(15) :> r; // check if some buttons are pressed
     waitMoment(10000000);
     if(r == 11) { // End Game button
        toUserAnt <: 15; // The end game code
        k = 0;
        break;
     } 
     else
        toUserAnt <: r; // send button pattern to userAnt
     playSound(200000,spkr);
     //b when pinseq(15) :> void;
     }
     printf("buttonListener ended\n");
}
/////////////////////////////////////////////////////////////////////////////////////////
//
// RELEVANT PART OF CODE TO EXPAND FOR YOU
//
/////////////////////////////////////////////////////////////////////////////////////////


//DEFENDER PROCESS... The defender is controlled by this process userAnt,
// which has channels to a buttonListener, visualiser and controller
void userAnt(chanend fromButtons, chanend toVisualiser, chanend toController) {
     unsigned int userAntPosition = 11; //the current defender position
     int buttonInput; //the input pattern from the buttonListener
     int attemptedAntPosition = 0; //the next attempted defender position after considering button
     int moveForbidden; //the verdict of the controller if move is allowed
     int k = 1;
     toVisualiser <: userAntPosition; //show initial position
     while (k == 1) {
         fromButtons :> buttonInput;
         if(buttonInput == 15) {
            toVisualiser <: 15;
            toController <: 15;
            k=0;
            break;
         }
         if (buttonInput == 14) attemptedAntPosition = userAntPosition + 1;
         if (buttonInput == 7) attemptedAntPosition = userAntPosition - 1;
         if(attemptedAntPosition > 11)
        	 attemptedAntPosition = 0;
         if(attemptedAntPosition < 0)
        	 attemptedAntPosition = 11;
        select {
            case toVisualiser :> attemptedAntPosition: {
                k = 0;
                break;
            }
            default : break;
        }

        toController <: attemptedAntPosition; // I send it to master, and see if it is eligible
        toController :> attemptedAntPosition;
        if(attemptedAntPosition == 14)
        	continue;
        if(attemptedAntPosition != 13 && attemptedAntPosition != 15 )
            userAntPosition = attemptedAntPosition;
        else
        	if (attemptedAntPosition == 15 )
        		k = 0;
        toVisualiser <: userAntPosition;
     }
     printf("User ended \n");
}


 //ATTACKER PROCESS... The attacker is controlled by this process attackerAnt,
// which has channels to the visualiser and controller
void attackerAnt(chanend toVisualiser, chanend toController) {
     int moveCounter = 0; //moves of attacker so far
     int t = 70000000;
     unsigned int attackerAntPosition = 5; //the current attacker position
     unsigned int attemptedAntPosition; //the next attempted position after considering move direction
     int currentDirection; //the current direction the attacker is moving
     int moveForbidden = 0; //the verdict of the controller if move is allowed
      //show initial position
     int k = 1;
     int i = 1;
     int random = rand();
     if(random % 2 == 0)
        currentDirection = 1;
    else
        currentDirection = 2;
     while (k == 1) {
    	 if (moveCounter % 31 == 0 || moveCounter % 37 == 0 || moveCounter % 43 == 0){
    		 if (currentDirection == 2)
    			 currentDirection = 1;
    		 else currentDirection = 2;
    	 }
		 if(attemptedAntPosition == 0 && currentDirection == 2) {
			 attemptedAntPosition = 11;
		 } else {
    	 if (currentDirection==1) { // Moving right (from 6 to 7)
    		 attemptedAntPosition = attackerAntPosition + 1;
    		 if(attemptedAntPosition > 11)
    			 attemptedAntPosition = 0;
    		 if(attemptedAntPosition < 0)
    			 attemptedAntPosition = 11;
    	 } else {
    	 if (currentDirection==2){ //Moving Left (from 6 to 5)
    		 attemptedAntPosition = attackerAntPosition - 1;
    		 if(attemptedAntPosition > 11)
    			 attemptedAntPosition = 0;
    	 } } } // Right now I should know the attempted Ant position

        toController <: attemptedAntPosition; // I send it to master, and see if it is eligible
        toController :> attemptedAntPosition;

        if(attemptedAntPosition != 13 && attemptedAntPosition != 15 )
            attackerAntPosition = attemptedAntPosition;
        if (attemptedAntPosition == 13){
        	 if (currentDirection == 2)
        	 	 currentDirection = 1;
        	 else currentDirection = 2;
        }
        else 
            if (attemptedAntPosition == 15){
            	if (currentDirection == 2)
            	   attackerAntPosition --;
            	else
            	   attackerAntPosition ++;
                toVisualiser <: attackerAntPosition;
            	k = 0;
                break;
            }
        moveCounter++;
        toVisualiser <: attackerAntPosition;
        waitMoment(t);
        if(t > 15000000 ) {
        	t -= moveCounter * 50000;
        }
     } //100000000
     printf("Attacker ended\n");
}

//COLLISION DETECTOR... the controller process responds to �permission-to-move� requests
// from attackerAnt and userAnt. The process also checks if an attackerAnt
// has moved to LED positions I, XII and XI.
void controller(chanend fromAttacker, chanend fromUser) {
     unsigned int lastReportedUserAntPosition = 11; //position last reported by userAnt
     unsigned int lastReportedAttackerAntPosition = 5; //position last reported by attackerAnt
     unsigned int attemptUser = 0;
     unsigned int attemptAttacker = 0;
     unsigned int userAttempt;
     unsigned int attackerAttempt;
     int k = 1;
     //int i = 0;
     fromUser :> attemptUser; //start game when user moves
     fromUser <: 14; //forbid first move

     while (k == 1) {
        select {
            case fromAttacker :> attemptAttacker: {
                if(attemptAttacker == lastReportedUserAntPosition) {
                    attemptAttacker = 13;
                    fromAttacker <: 13;
                    break;
                }
                if (attemptAttacker == 11 || attemptAttacker == 10 || attemptAttacker == 0){
                	attemptAttacker = 15;
                	fromAttacker <: 15;
                    lastReportedAttackerAntPosition = 15;
                    //fromUser :> attemptAttacker;
                    //fromUser <: 15;
                    k = 0;
                	break;
                }
                fromAttacker <: attemptAttacker;
                lastReportedAttackerAntPosition = attemptAttacker;
                break;
             	}
            case fromUser :> attemptUser: {
            	//printf("Received by controller: %d\n",attempt);
                if(attemptUser == lastReportedAttackerAntPosition) {
                    attemptUser = 13;
                    fromUser <: 13;
                    break;
                }
                if (attemptUser == 15) {
                	//fromUser <: 15;
                    lastReportedUserAntPosition = 15;
                    k = 0;
                    break;
                }
                fromUser <: attemptUser;
                lastReportedUserAntPosition = attemptUser;
                break;
            }
        }
        if(lastReportedUserAntPosition == 15 || lastReportedAttackerAntPosition == 15) {
            k = 0;
            break;
        }
     }
     printf("Controller ended\n");
}

//MAIN PROCESS defining channels, orchestrating and starting the processes
int main(void) {
     chan buttonsToUserAnt, //channel from buttonListener to userAnt
     userAntToVisualiser, //channel from userAnt to Visualiser
     attackerAntToVisualiser, //channel from attackerAnt to Visualiser
     attackerAntToController, //channel from attackerAnt to Controller
     userAntToController; //channel from userAnt to Controller
     chan quadrant0,quadrant1,quadrant2,quadrant3; //helper channels for LED visualisation


 par{
 	 //PROCESSES FOR YOU TO EXPAND
     //on stdcore[0]: changeLed();
     on stdcore[1]: userAnt(buttonsToUserAnt,userAntToVisualiser,userAntToController);
     on stdcore[2]: attackerAnt(attackerAntToVisualiser,attackerAntToController);
     on stdcore[3]: controller(attackerAntToController, userAntToController);

     //HELPER PROCESSES
     on stdcore[0]: buttonListener(buttons, speaker,buttonsToUserAnt);
     on stdcore[0]: visualiser(userAntToVisualiser,attackerAntToVisualiser,quadrant0,quadrant1,quadrant2,quadrant3, 1);
     on stdcore[0]: showLED(cled0,quadrant0);
     on stdcore[1]: showLED(cled1,quadrant1);
     on stdcore[2]: showLED(cled2,quadrant2);
     on stdcore[3]: showLED(cled3,quadrant3);
     }
    return 0;
}
